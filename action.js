// const data = {
//     value: 10,
//     state: {
//         value:5,
//         state:{
//             value:2,
//             state:{
//                 value:9,
//                 state: {
//                     value:7,
//                     state: null
//                 }
//             }
//         }
//     }
// }


// function getSumm(obj) {
//     // let summ =0;
//     if(obj.state) {
//     return obj.value + getSumm(obj.state);
//     }
//     return obj.value;
// }

// const summ = getSumm(data);
// console.log(summ);



const videoTag = document.querySelector('#video');
const playBtn = document.querySelector('#play');
const pauseBtn = document.querySelector("#pause");

const durationSpan = document.querySelector('.duration');
const currentSpan = document.querySelector('.currentTime');

playBtn.addEventListener('click', playVideo);

let currentTimeInterval;

pauseBtn.addEventListener('click', pauseVideo);

function getDuration() {
const duration = videoTag.duration;
const minutes = Math.round(duration/60);
const seconds = Math.round(duration-minutes*60);
durationSpan.innerText = `${minutes > 9 ? minutes : '0' + minutes } : ${seconds > 9 ? seconds : '0' + seconds}`;
}

// setTimeout(() => {
//     console.log(videoTag.currentTime);
//     console.log(videoTag.duration);
// })

videoTag.onloadedmetadata = getDuration;


function playVideo() {
    videoTag.play();
   currentTimeInterval = setInterval(() => {
        currentSpan.innerText = videoTag.currentTime;
        console.log(1);
     
     }, 1000);
     
}


function pauseVideo() {
    videoTag.pause();
    clearInterval(currentTimeInterval);
}